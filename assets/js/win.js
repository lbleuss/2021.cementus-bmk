var winState = {
    create:function(){

        //display text
        var winLabel = game.add.text(80,80, 'DU HAST ALLE ZEMENTSÄCKE GESAMMELT!',
                                    {font: '18px Press Start 2P', fill: '#000000'});

         var startLabel = game.add.text(80,game.world.height-80, 'DRÜCKE DIE "ENTER" TASTE UM ERNEUT ZU SPIELEN.',
                                    {font: '15px Press Start 2P', fill:'#000000'});
        
        //triggers function on press of Enter Key
        var enterkey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        //specifies which function
        enterkey.onDown.addOnce(this.start,this);
    },
    start: function()  {
        score-= 12; //reset score on play again
        game.state.start('play');
    },
};