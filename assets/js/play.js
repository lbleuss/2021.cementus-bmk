var score = 0;
var playState = {


create: function(){
    //add background
    game.add.sprite(0, 0, 'background');

    
    
    // platform group start
    platforms = game.add.group();
    platforms.enableBody = true;

    // ground function
    var ground = platforms.create(0, game.world.height - 64, 'ground');
    ground.scale.setTo(2, 2);

    //  When set to false platforms drop on contact
    ground.body.immovable = true;

    //  creates ledges/platforms
    var ledge = platforms.create(320, 400, 'platform');
    
    ledge.body.immovable = true;

    ledge = platforms.create(-150, 250, 'platform');

    ledge.body.immovable = true;

    ledge = platforms.create(450, 140, 'platform');

    ledge.body.immovable = true;

    
    
   
    //item group start
    bags = game.add.group();

    bags.enableBody = true;

    //  Here we'll create 12 of them evenly spaced apart
    for (var i = 0.5; i < 12.5; i++) //difference needs to be 12 - setting first value to 0 makes bag spawn at the edge of the map
    {
        
        var bag = bags.create(i * 65, 0, 'item'); // first value determines spacing on the x-axis y-axis doesn't matter for this game so I set it to 0.

        //  enable gravity
        bag.body.gravity.y = 100;

        //  Assigns random bounce to items first number between 0 (no bounce) and 1 (max bounce)
        bag.body.bounce.y = 0.05 + Math.random() * 0.2;
    }
    
    
    /* player */

    player = game.add.sprite(32, game.world.height - 150, 'pc'); //adds sprite

    game.physics.arcade.enable(player); //enable physics for player

    player.body.bounce.y = 0.2;
    player.body.gravity.y = 300;
    player.body.collideWorldBounds = true; //makes it so player cannot go out of bounds

    player.animations.add('left', [0, 1, 2, 3], 10, true);
    player.animations.add('right', [5, 6, 7, 8], 10, true);



    //controls

    cursors = game.input.keyboard.createCursorKeys();

},
    update: function(){
                         var hitPlatform = game.physics.arcade.collide(player, platforms); //enables collision between player and platform
        
                              game.physics.arcade.collide(bags, platforms); //enables collision between item and platform
        
              
        
                              game.physics.arcade.overlap(player, bags, collectBag, null, this); //checks whether player overlaps with item
        
        
              player.body.velocity.x = 0; //resets movement
        
              if (cursors.left.isDown)
              {
                  
                  player.body.velocity.x = -150;   //  Move to the left
          
                  player.animations.play('left');
              }
              else if (cursors.right.isDown)
              {
                  
                  player.body.velocity.x = 150;   //  Move to the right
          
                  player.animations.play('right');
              }
              else
              {
                  
                  player.animations.stop();  //  idle
          
                  player.frame = 4;
              }
          
              
              if (cursors.up.isDown && player.body.touching.down && hitPlatform) //  Allow the player to jump if they are touching the ground.
              {
                  player.body.velocity.y = -320;  //number sets jump height minimum -300 to reach all platforms | recommended -320
              }
        
        
        
        
        function collectBag (player, bag) {
            //specifies amount added to score
            score+= 1 ;
            // Removes the bag from the screen
            bag.kill();
            console.log('Bags collected ' + score); 
            //specifies score needed to win game
            if(score == 12){
                game.state.start('win');
            }
            
                
            
        }
    },
   
};
